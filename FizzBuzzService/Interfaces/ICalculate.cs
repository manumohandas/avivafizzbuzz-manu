﻿// <copyright file = "ICalculate.cs" company="TCS"> 
// TCS Bangalore. All rights reserved </copyright>
// <author>Manu Mohandas</author>

namespace FizzBuzzService
{
    using System;

    /// <summary>
    /// Defines the method to check divisibility and to print output.
    /// </summary>
    public interface ICalculate
    {
        /// <summary>
        /// To check the divisibility of the input number
        /// </summary>
        /// <param name="number">Input number</param>
        /// <returns>True or False</returns>
        bool IsDivisible(int number);

        /// <summary>
        /// Returns a string value based on the divisibility of the number
        /// </summary>
        /// <param name="number">The input number.</param>        
        /// <returns>String equivalent of number</returns>
        string Output(int number, DayOfWeek dayofweek);
    }
}
