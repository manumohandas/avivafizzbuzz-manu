﻿// <copyright file = "IFizzBuzzService.cs" company="TCS"> 
// TCS Bangalore. All rights reserved </copyright>
// <author>Manu Mohandas</author>

namespace FizzBuzzService
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Interface that defines the method to build the series.
    /// </summary>
    public interface IFizzBuzzService
    {
        /// <summary>
        /// Method to build the series.
        /// </summary>
        /// <param name="maxNumber">The maximum number of the series.</param>
        /// <returns>The series.</returns>
        List<string> GetFizzBuzzSeries(int maxNumber);
    }
}
