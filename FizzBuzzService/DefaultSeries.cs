﻿// <copyright file = "DefaultSeries.cs" company="TCS"> 
// TCS Bangalore. All rights reserved </copyright>
// <author>Manu Mohandas</author>

namespace FizzBuzzService
{
    using System;

    /// <summary>
    /// Class to get the default value of the series.
    /// </summary>
    public class DefaultSeries : ICalculate
    {
        /// <summary>
        ///  To check the divisibility of the input number
        /// </summary>
        /// <param name="number">Input number</param>
        /// <returns>True or False</returns>
        public bool IsDivisible(int number)
        {
            return number % 3 != 0 && number % 5 != 0;
        }

        /// <summary>
        /// Returns a string value based on the divisibility of the number
        /// </summary>
        /// <param name="number">Input number</param>        
        /// <returns>The string equivalent</returns>
        public string Output(int number, DayOfWeek dayofweek)
        {
            return number.ToString();
        }
    }
}
