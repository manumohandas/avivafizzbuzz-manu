﻿// <copyright file = "FizzBuzzService.cs" company="TCS"> 
// TCS Bangalore. All rights reserved </copyright>
// <author>Manu Mohandas</author>

namespace FizzBuzzService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// The service class.
    /// </summary>
    public class FizzBuzzService : IFizzBuzzService
    {
        /// <summary>
        /// List of different objects.
        /// </summary>
        private IList<ICalculate> allCalculates;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzService"/> class.
        /// </summary>
        /// <param name="calculates">List of rules.</param>
        public FizzBuzzService(ICalculate[] calculates)
        {
            this.allCalculates = calculates.ToList<ICalculate>();
        }

        /// <summary>
        /// Method to build the series
        /// </summary>
        /// <param name="maxNumber">The last number for the series</param>
        /// <returns>Returns the series</returns>
        public List<string> GetFizzBuzzSeries(int maxNumber)
        {
            var resultList = new List<string>();
            for (int element = 1; element <= maxNumber; element++)
            {
                resultList.Add(this.allCalculates.First(a => a.IsDivisible(element)).Output(element, DateTime.Now.DayOfWeek));
            }

            return resultList;
        }
    }
}
