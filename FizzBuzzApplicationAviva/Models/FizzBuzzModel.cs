﻿// <copyright file = "FizzBuzzModel.cs" company="TCS"> 
// TCS Bangalore. All rights reserved </copyright>
// <author>Manu Mohandas</author>

namespace FizzBuzzApplicationAviva.Models
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using PagedList;

    /// <summary>
    /// Model class for the view.
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Gets or sets user input.
        /// </summary>
        [DisplayName("Enter Number")]
        [Required(ErrorMessage = "Please enter valid integer between 1 and 1000")]
        [Range(1, 1000, ErrorMessage = "Please enter valid integer between 1 and 1000")]
        public int? InputNumber { get; set; }

        /// <summary>
        /// Gets or sets the series.
        /// </summary>
        public PagedList<string> FizzBuzzSeries { get; set; }
    }
}