﻿// <copyright file = "FizzBuzzController.cs" company="TCS"> 
// TCS Bangalore. All rights reserved </copyright>
// <author>Manu Mohandas</author>

namespace FizzBuzzApplicationAviva.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using FizzBuzzApplicationAviva.Models;
    using FizzBuzzService;
    using PagedList;

    /// <summary>
    /// The controller class.
    /// </summary>
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// Service reference
        /// </summary>
        private readonly IFizzBuzzService fizzBuzzService;

        /// <summary>
        /// Records per page in the view
        /// </summary>
        private readonly int recordsPerPage;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzController"/> class.
        /// </summary>
        /// <param name="fzBuzzService">The service class.</param>
        public FizzBuzzController(IFizzBuzzService fzBuzzService)
        {
            this.fizzBuzzService = fzBuzzService;
            this.recordsPerPage = 20;        
        }

        /// <summary>
        /// The Index method
        /// </summary>       
        /// <returns>The view result</returns>
        public ActionResult Index()
        {
            var model = new FizzBuzzModel();
            model.FizzBuzzSeries = new PagedList<string>(new List<string>(), 1, this.recordsPerPage);
            return this.View("FizzBuzzSeries", model);
        }

        /// <summary>
        /// The Action method
        /// </summary>
        /// <param name="model">The model for the view</param>
        /// <returns>The view result</returns>
        [HttpGet]
        public ActionResult GetSeries(FizzBuzzModel model, int? page)
        {
            var result = new List<string>();
            var maxNumber = (int)model.InputNumber;
            if (ModelState.IsValid)
            {
                result = this.fizzBuzzService.GetFizzBuzzSeries((int)model.InputNumber);
            }

            int pageNumber = page ?? 1;
            var pagedList = new PagedList<string>(result, pageNumber, this.recordsPerPage);
            model.FizzBuzzSeries = pagedList;
            return this.View("FizzBuzzSeries", model);
        }
    }
}