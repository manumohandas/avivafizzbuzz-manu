﻿
namespace FizzBuzzApplicationTest
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzzApplicationAviva.Controllers;
    using FizzBuzzApplicationAviva.Models;
    using NUnit.Framework;
    using Moq;
    using FizzBuzzService;
    using PagedList;

    [TestFixture]
    public class FizzBuzzControllerTest
    {
        /// <summary>
        /// test controller.
        /// </summary>
        private FizzBuzzController fizzbuzzController;

        /// <summary>
        /// service object.
        /// </summary>
        private Mock<IFizzBuzzService> mockService;

        /// <summary>
        /// Configuration setup.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.mockService = new Mock<IFizzBuzzService>();
            this.mockService.Setup(a => a.GetFizzBuzzSeries(It.IsAny<int>())).Returns(this.GetDummySeries());
            this.fizzbuzzController = new FizzBuzzController(this.mockService.Object);
        }

        /// <summary>
        /// Method to test twenty records are displayed in a page
        /// </summary>
        [Test]
        public void OnUserInput_WhenValidInput_ReturnCorrectNumberOfElements()
        {
            var model = new FizzBuzzModel { InputNumber = 22 };
            var result = this.fizzbuzzController.GetSeries(model,null) as ViewResult;
            var resultModel = (FizzBuzzModel)result.ViewData.Model;
            var firstSeries = (PagedList<string>)resultModel.FizzBuzzSeries;
            Assert.AreEqual(20, firstSeries.Count);
        }

        /// <summary>
        /// Method to create dummy series
        /// </summary>
        /// <returns>dummy series</returns>
        private List<string> GetDummySeries()
        {
            return new List<string>()
            {
                "1",
                "2",
                "Fizz",
                "4",
                "Buzz",
                "7",
                "8",
                "Fizz",
                "Buzz",
                "11",
                "Fizz",
                "13",
                "14",
                "FizzBuzz",
                "16",
                "17",
                "Fizz",
                "19",
                "Buzz",
                "Fizz",
                "22"
            };
        }
    }
}
