﻿// <copyright file = "DivisibilityByThreeAndFiveUnitTest.cs" company="TCS"> 
// TCS Bangalore. All rights reserved </copyright>
// <author>Manu Mohandas</author>

namespace FizzBuzzServiceTest
{
    using System;
    using FizzBuzzService;
    using NUnit.Framework;

    /// <summary>
    /// Class to test the divisibility by three and Five.
    /// </summary>
    [TestFixture]
    public class DivisibilityByThreeAndFiveUnitTest
    {
        /// <summary>
        /// Calculator class.
        /// </summary>
        private ICalculate divideByThreeAndFiveRule;

        /// <summary>
        /// Configuration method.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.divideByThreeAndFiveRule = new DivisibilityByThreeAndFive();
        }

        /// <summary>
        /// Test to verify the number is divisible by three and five.
        /// </summary>
        [Test]
        public void WhenNumberDivisibleByThreeAndFive_ShouldReturnTrue()
        {
            Assert.IsTrue(this.divideByThreeAndFiveRule.IsDivisible(15));
        }

        /// <summary>
        /// Test to verify the number is not divisible by three and five.
        /// </summary>
        [Test]
        public void WhenNumberNotDivisibleByThreeAndFive_ShouldReturnFalse()
        {
            Assert.IsFalse(this.divideByThreeAndFiveRule.IsDivisible(4));
        }

        /// <summary>
        /// Test if the method returns a correct string.
        /// </summary>
        [Test]
        public void WhenNumberDivisibleByThreeAndFive_ReturnFizzBuzz()
        {
            Assert.AreEqual("FizzBuzz", this.divideByThreeAndFiveRule.Output(15, DayOfWeek.Monday));
        }

        /// <summary>
        /// Test if the method returns a correct string.
        /// </summary>
        [Test]
        public void WhenNumberDivisibleByThreeAndFive_ReturnWizzWuzz()
        {
            Assert.AreEqual("WizzWuzz", this.divideByThreeAndFiveRule.Output(15, DayOfWeek.Wednesday));
        }
    }
}

