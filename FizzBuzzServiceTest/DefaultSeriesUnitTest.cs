﻿// <copyright file = "DefaultSeriesUnitTest.cs" company="TCS"> 
// TCS Bangalore. All rights reserved </copyright>
// <author>Manu Mohandas</author>

namespace FizzBuzzServiceTest
{
    using System;
    using FizzBuzzService;
    using NUnit.Framework;

    /// <summary>
    /// Class to test the default series.
    /// </summary>
    [TestFixture]
    public class DefaultSeriesUnitTest
    {
        /// <summary>
        /// Calculator class.
        /// </summary>
        private ICalculate defaultSeriesRule;

        /// <summary>
        /// Configuration method.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.defaultSeriesRule = new DefaultSeries();
        }

        /// <summary>
        /// Test to verify the number is not divisible by 3 and 5
        /// </summary>
        [Test]
        public void WhenNumberNotDivisibleByThreeAndFive_IsDivisibleShouldReturnTrue()
        {
            Assert.IsTrue(this.defaultSeriesRule.IsDivisible(11));
        }

        /// <summary>
        /// Test to verify the number is not divisible by 3 and 5
        /// </summary>
        [Test]
        public void WhenNumberDivisibleByThreeAndFive_IsDivisibleShouldReturnFalse()
        {
            Assert.IsFalse(this.defaultSeriesRule.IsDivisible(15));
        }

        /// <summary>
        /// Test to verify the method returns correct string.
        /// </summary>
        [Test]
        public void WhenNumberNotDivisibleByThreeAndFive_ReturnNumberAsString()
        {
            Assert.AreEqual("4", this.defaultSeriesRule.Output(4, DayOfWeek.Monday));
        }
    }
}
