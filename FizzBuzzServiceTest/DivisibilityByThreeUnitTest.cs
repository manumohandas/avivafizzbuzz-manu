﻿// <copyright file = "DivisibilityByThreeUnitTest.cs" company="TCS"> 
// TCS Bangalore. All rights reserved </copyright>
// <author>Manu Mohandas</author>

namespace FizzBuzzServiceTest
{
    using System;
    using FizzBuzzService;
    using NUnit.Framework;

    /// <summary>
    /// Class to test the divisibility by three.
    /// </summary>
    [TestFixture]
    public class DivisibilityByThreeUnitTest
    {
        /// <summary>
        /// Calculator class.
        /// </summary>
        private ICalculate divideByThreeRule;

        /// <summary>
        /// Configuration method.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.divideByThreeRule = new DivisibilityByThree();
        }

        /// <summary>
        /// Test to verify the number is divisible by three.
        /// </summary>
        [Test]
        public void WhenNumberDivisibleByThree_ShouldReturnTrue()
        {
            Assert.IsTrue(this.divideByThreeRule.IsDivisible(3));
        }

        /// <summary>
        /// Test to verify the number is not divisible by three.
        /// </summary>
        [Test]
        public void WhenNumberNotDivisibleByThree_ShouldReturnFalse()
        {
            Assert.IsFalse(this.divideByThreeRule.IsDivisible(4));
        }

        /// <summary>
        /// Test if the method returns a correct string.
        /// </summary>
        [Test]
        public void WhenNumberDivisibleByThree_ReturnFizz()
        {
            Assert.AreEqual("Fizz", this.divideByThreeRule.Output(3, DayOfWeek.Monday));
        }

        /// <summary>
        /// Test if the method returns a correct string.
        /// </summary>
        [Test]
        public void WhenNumberDivisibleByThree_ReturnWizz()
        {
            Assert.AreEqual("Wizz", this.divideByThreeRule.Output(3, DayOfWeek.Wednesday));
        }
    }
}
