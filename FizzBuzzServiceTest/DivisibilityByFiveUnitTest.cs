﻿// <copyright file = "DivisibilityByFiveUnitTest.cs" company="TCS"> 
// TCS Bangalore. All rights reserved </copyright>
// <author>Manu Mohandas</author>

namespace FizzBuzzServiceTest
{
    using System;
    using FizzBuzzService;
    using NUnit.Framework;

    /// <summary>
    /// Class to test the divisibility by five.
    /// </summary>
    [TestFixture]
    public class DivisibilityByFiveUnitTest
    {
        /// <summary>
        /// Calculator class.
        /// </summary>
        private ICalculate divideByFiveRule;

        /// <summary>
        /// Configuration method.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.divideByFiveRule = new DivisibilityByFive();
        }

        /// <summary>
        /// Test to verify the number is divisible by five.
        /// </summary>
        [Test]
        public void WhenNumberDivisibleByFive_ShouldReturnTrue()
        {
            Assert.IsTrue(this.divideByFiveRule.IsDivisible(5));
        }

        /// <summary>
        /// Test to verify the number is not divisible by five.
        /// </summary>
        [Test]
        public void WhenNumberNotDivisibleByFive_ShouldReturnFalse()
        {
            Assert.IsFalse(this.divideByFiveRule.IsDivisible(4));
        }

        /// <summary>
        /// Test if the method returns a correct string.
        /// </summary>
        [Test]
        public void WhenNumberDivisibleByFive_ReturnBuzz()
        {
            Assert.AreEqual("Buzz", this.divideByFiveRule.Output(5, DayOfWeek.Monday));
        }

        /// <summary>
        /// Test if the method returns a correct string.
        /// </summary>
        [Test]
        public void WhenNumberDivisibleByFive_ReturnWuzz()
        {
            Assert.AreEqual("Wuzz", this.divideByFiveRule.Output(5, DayOfWeek.Wednesday));
        }
    }
}
