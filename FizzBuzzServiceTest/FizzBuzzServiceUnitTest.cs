﻿// <copyright file = "FizzBuzzServiceUnitTest.cs" company="TCS"> 
// TCS Bangalore. All rights reserved </copyright>
// <author>Manu Mohandas</author>

namespace FizzBuzzServiceTest
{
    using System;
    using System.Collections.Generic;
    using FizzBuzzService;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class to test the service object.
    /// </summary>
    [TestFixture]
    public class FizzBuzzServiceUnitTest
    {
        /// <summary>
        /// The service class object.
        /// </summary>
        private FizzBuzzService fbService;

        /// <summary>
        /// Mock object for divisibility by three
        /// </summary>
        private Mock<ICalculate> mockdivisibleByThree;

        /// <summary>
        /// Mock object for divisibility by five
        /// </summary>
        private Mock<ICalculate> mockdivisibleByFive;

        /// <summary>
        /// Mock object for divisibility by three and five
        /// </summary>
        private Mock<ICalculate> mockdivisibleByThreeAndFive;

        /// <summary>
        /// Mock object for default elements of the series.
        /// </summary>
        private Mock<ICalculate> mockdefaultSeries;

        /// <summary>
        /// Configuration set up.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.mockdivisibleByThree = new Mock<ICalculate>();
            this.mockdivisibleByThree.Setup(a => a.IsDivisible(It.IsAny<int>())).Returns(false);
            this.mockdivisibleByFive = new Mock<ICalculate>();
            this.mockdivisibleByFive.Setup(a => a.IsDivisible(It.IsAny<int>())).Returns(false);
            this.mockdivisibleByThreeAndFive = new Mock<ICalculate>();
            this.mockdivisibleByThreeAndFive.Setup(a => a.IsDivisible(It.IsAny<int>())).Returns(false);
            this.mockdefaultSeries = new Mock<ICalculate>();
            this.mockdefaultSeries.Setup(a => a.IsDivisible(It.IsAny<int>())).Returns(true);
            this.mockdefaultSeries.Setup(a => a.Output(It.IsAny<int>(), It.IsAny<DayOfWeek>())).Returns(7.ToString());

            var calculates = new List<ICalculate>
            {
                this.mockdivisibleByThree.Object,
                this.mockdivisibleByFive.Object,
                this.mockdivisibleByThreeAndFive.Object,
                this.mockdefaultSeries.Object
            };

            this.fbService = new FizzBuzzService(calculates.ToArray());
        }

        /// <summary>
        /// test to verify number of items in list is correct.
        /// </summary>
        [Test]
        public void OnUserInput_OnValidInput_ListHasCorrectNumberOfElements()
        {
            var resultList = new List<string>(this.fbService.GetFizzBuzzSeries(7));
            Assert.AreEqual(7, resultList.Count);
        }
    }
}
